package Week2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class week2_main extends Thread {

	static ArrayList<UserClass> _User;
	static ArrayList<BookController> _ArrBook;
	static UserController user_ = new UserController();

	public static void Login(ArrayList<UserClass> user, ArrayList<BookController> arrBook) {
		Scanner sc = new Scanner(System.in);

		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();

			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName()) && passString.equals(user.get(i).getPassword())) {
					user_.LogFile(userString + " has log in");
					user_.Menu(arrBook, user, userString);
				} else {
					tt = false;

				}
			}
			if (tt == false) {
				user_.LogFile("wrong user or password for user: " + userString);
				System.out.println("404");
			}
		} while (tt == false);
		sc.close();
	}

	public ArrayList<UserClass> ReadFileUser(ArrayList<UserClass> ListUser) {
		String filename = "C:\\Users\\pham.minh\\eclipse-workspace\\Week2_testAll\\DataUser.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					UserClass userAtributes = new UserClass(lineString);
					ListUser.add(userAtributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListUser;
	}

	public ArrayList<BookController> ReadFileBook(ArrayList<BookController> ListBook) {
		String filename = "C:\\Users\\pham.minh\\eclipse-workspace\\Week2_testAll\\DataBook.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					BookController bookAttributes = new BookController(lineString);
					ListBook.add(bookAttributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListBook;
	}

	public static void main(String[] args) throws SecurityException, IOException {
		week2_main week2 = new week2_main();

		ArrayList<BookController> arrBoookAtributes = new ArrayList<>();
		arrBoookAtributes = week2.ReadFileBook(arrBoookAtributes);
		_ArrBook = arrBoookAtributes;

		user_.LogFile("Start");

		ArrayList<UserClass> arruserAtributes = new ArrayList<>();
		arruserAtributes = week2.ReadFileUser(arruserAtributes);
		_User = arruserAtributes;
		week2.start();

	}

	@Override
	public void run() {
		Login(this._User, this._ArrBook);

	}

}
